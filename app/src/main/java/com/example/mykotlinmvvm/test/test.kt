package com.example.mykotlinmvvm.test

fun main() {

    myData(10)
}

inline fun <reified T> myData(args : T) {
    println("args ${T::class.java}")
}

