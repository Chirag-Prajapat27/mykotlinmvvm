package com.example.mykotlinmvvm.util

import android.graphics.drawable.Drawable
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.example.mykotlinmvvm.databinding.model.ProgramingModel
import com.squareup.picasso.Picasso
import java.util.*

@BindingAdapter("setPrice")
fun TextView.setPrice(price:String){
    setText("₹ "+price)
}

@BindingAdapter("imageUrl", "placeholder")
fun ImageView.loadImage(url: String, placeholder: Drawable) {
    Picasso.get().load(url).placeholder(placeholder).into(this)
}

@BindingAdapter("capitalizeFirst")
fun TextView.capitalizeFirst(value: String) {
    text = value.replaceFirstChar { if (it.isLowerCase()) it.titlecase(Locale.getDefault()) else it.toString() }
}

