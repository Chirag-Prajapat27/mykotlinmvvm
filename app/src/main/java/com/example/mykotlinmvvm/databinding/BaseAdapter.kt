package com.example.mykotlinmvvm.databinding

import android.view.LayoutInflater
import android.view.ViewGroup

import android.widget.TextView
//import androidx.databinding.library.baseAdapters.BR
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.mykotlinmvvm.BR
import com.example.mykotlinmvvm.R
import com.example.mykotlinmvvm.databinding.BaseAdapter.MyViewHolder
import com.example.mykotlinmvvm.databinding.model.ProgramingModel

class BaseAdapter : ListAdapter<ProgramingModel, MyViewHolder>(DiffUtil())  {


//    class MyViewHolder(view: View) : RecyclerView.ViewHolder(view){
//        val name = view.findViewById<TextView>(R.id.tvName)
    var update : ((id:String?)-> Unit)? = null

   class MyViewHolder(binding: ItemProgramBinding) : RecyclerView.ViewHolder(binding.root){  // DataBinding BR

       val myBinding = binding

    }

    class DiffUtil : androidx.recyclerview.widget.DiffUtil.ItemCallback<ProgramingModel>() {
        override fun areItemsTheSame(oldItem: ProgramingModel, newItem: ProgramingModel): Boolean {

            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(
            oldItem: ProgramingModel,
            newItem: ProgramingModel
        ): Boolean {
            return oldItem == newItem
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
//        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_program,parent,false)

        val view = ItemProgramBinding.inflate(LayoutInflater.from(parent.context),parent,false)//Data Binding

        return MyViewHolder(view)
    }


    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        update?.invoke("2")
        holder.myBinding.myName = getItem(position)

    }
}