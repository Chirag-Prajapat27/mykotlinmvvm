package com.example.mykotlinmvvm.databinding.model

data class ProgramingModel(val id:Int, val name: String)