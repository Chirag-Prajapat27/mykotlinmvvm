package com.example.mykotlinmvvm.databinding

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.example.mykotlinmvvm.R
import com.example.mykotlinmvvm.mvvmretrofit.ViewModelRetrofit
import com.example.mykotlinmvvm.roomdb.AdapterEmployee
import com.example.mykotlinmvvm.roomdb.DataBaseEmployee
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch



class ViewDataFragment : Fragment() {

    private lateinit var binding: FragmentViewDataBinding
    lateinit var database : DataBaseEmployee
    lateinit var viewModel: ViewModelRetrofit

    @OptIn(DelicateCoroutinesApi::class)
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_view_data,container,false)
        val view = ViewModelProvider(this)[MyViewModel::class.java]
        binding.dataViewModel = view
        binding.lifecycleOwner = this
        binding.dataFragment = this


//        database = DataBaseEmployee.getDataBase(requireContext())
//        GlobalScope.launch {
//            database.daoEmployee().insertEmployeeData(EmployeeModel("Chirag",18000))
//        }

        return binding.root
    }

    fun callApi(){
        CoroutineScope(Dispatchers.IO).launch {
            database.daoEmployee().getEmployeeData().observe(viewLifecycleOwner) {
                val adapter = AdapterEmployee(it)
                binding.rvViewData.adapter = adapter

                adapter.notifyDataSetChanged()
            }

        }
    }

}