package com.example.mykotlinmvvm.databinding

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.navigation.NavController
import androidx.navigation.NavDirections

class MyViewModel() : ViewModel() {

    var data = "chirag"
    val myLiveData =
        MutableLiveData("knowing is not enough, we must apply. willing is not enough, we must do")

    fun updateQuote() {
        myLiveData.value = "do or Die"
    }

    fun navigate(navDirection: NavDirections) {
        val navController : NavController


    }
}