package com.example.mykotlinmvvm.shareprefrencesdagger2

import android.content.SharedPreferences
import javax.inject.Inject
import javax.inject.Singleton


class SessionRepository @Inject constructor(val sp : SharedPreferences) {

    fun saveUser(name: String?) {
        sp.edit().putString("user",name).apply()
    }

    fun getUser() : String? {
        return sp.getString("user","na")
    }
}