package com.example.mykotlinmvvm.shareprefrencesdagger2

import androidx.lifecycle.ViewModel
import javax.inject.Inject

class SessionViewModel @Inject constructor(val repository: SessionRepository) : ViewModel() {

    fun setUser(user:String?) = repository.saveUser(user)

    fun getUser() = repository.getUser()
}
