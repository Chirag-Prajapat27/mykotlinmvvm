package com.example.mykotlinmvvm.shareprefrencesdagger2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import com.example.mykotlinmvvm.App
import com.example.mykotlinmvvm.R
import com.example.mykotlinmvvm.retrofitwithdi.di.VIewModelFactorySp
import com.example.mykotlinmvvm.test.MyCar
import javax.inject.Inject

class SPWithDagger2Activity : AppCompatActivity() {
    @Inject
    lateinit var factorySp: VIewModelFactorySp

    @Inject
    lateinit var myCar : MyCar

    @Inject
    lateinit var myCar2: MyCar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_spwith_dagger2)

//        val component = DaggerSPComponents.factory().create(this)
        val component = (application as App).spComponents
        component.inject(this)

        val spViewModel : SessionViewModel = ViewModelProvider(this,factorySp)[SessionViewModel::class.java]
        spViewModel.setUser("Chirag")

        Toast.makeText(this, "${spViewModel.getUser()}", Toast.LENGTH_SHORT).show()
    }
}
