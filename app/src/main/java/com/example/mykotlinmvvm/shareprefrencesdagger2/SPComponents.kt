package com.example.mykotlinmvvm.shareprefrencesdagger2

import android.content.Context
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [StorageModule::class])
interface SPComponents {

    fun inject(activity: SPWithDagger2Activity)

    @Component.Factory
    interface Factory{
        fun create(@BindsInstance context: Context): SPComponents
    }

}