package com.example.mykotlinmvvm.shareprefrencesdagger2

import android.content.Context
import android.content.SharedPreferences
import dagger.Module
import dagger.Provides

@Module
class StorageModule {

    @Provides
    fun provideSessionManager(context: Context) : SharedPreferences {
        return context.getSharedPreferences("mySp",Context.MODE_PRIVATE)
    }



}