package com.example.mykotlinmvvm.retrofitwithdi

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.example.mykotlinmvvm.R
import com.example.mykotlinmvvm.retrofitwithdi.di.DaggerRetrofitComponentDI
import javax.inject.Inject

class RetrofitWithDIActivity : AppCompatActivity() {

    @Inject
    lateinit var repository: RetroRepositoryDI

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_retrofit_with_diactivity)

        val component = DaggerRetrofitComponentDI.builder().build()

        component.inject(this)

        val viewModel = ViewModelProvider(this, ViewModelFactoryRetroDI(repository))[ViewModelRetrofitDI::class.java]

        viewModel.getQuotes().observe(this) {
            Toast.makeText(this, "$it", Toast.LENGTH_SHORT).show()
        }
    }
}