package com.example.mykotlinmvvm.retrofitwithdi

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import javax.inject.Inject

class ViewModelFactoryRetroDI @Inject constructor(val repository: RetroRepositoryDI ) :ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return ViewModelRetrofitDI(repository) as T

    }

}