package com.example.mykotlinmvvm.retrofitwithdi

import com.example.mykotlinmvvm.mvvmretrofit.QuoteModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject


class RetroRepositoryDI @Inject constructor(val serviceDi: RetrofitServiceDi) {

    suspend fun getAllQuotes(): Flow<QuoteModel> = flow {
        val response = serviceDi.getQuotes()
        emit(response)
    }

}