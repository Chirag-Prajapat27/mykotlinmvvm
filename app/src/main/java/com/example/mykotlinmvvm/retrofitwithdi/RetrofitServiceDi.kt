package com.example.mykotlinmvvm.retrofitwithdi

import com.example.mykotlinmvvm.mvvmretrofit.QuoteModel
import retrofit2.http.GET

interface RetrofitServiceDi {

    @GET("/quotes")
    suspend fun getQuotes(): QuoteModel
}