package com.example.mykotlinmvvm.retrofitwithdi.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.mykotlinmvvm.shareprefrencesdagger2.SessionRepository
import com.example.mykotlinmvvm.shareprefrencesdagger2.SessionViewModel
import javax.inject.Inject

class VIewModelFactorySp @Inject constructor(val repository: SessionRepository) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return SessionViewModel(repository) as T
    }
}