package com.example.mykotlinmvvm.retrofitwithdi.di

import com.example.mykotlinmvvm.retrofitwithdi.RetroRepositoryDI
import com.example.mykotlinmvvm.retrofitwithdi.RetrofitWithDIActivity
import dagger.Component

@Component(modules = [RetrofitModule::class])
interface RetrofitComponentDI {

    fun inject(activity:RetrofitWithDIActivity)

}