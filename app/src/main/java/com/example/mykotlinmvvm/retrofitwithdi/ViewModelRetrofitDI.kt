package com.example.mykotlinmvvm.retrofitwithdi

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import kotlinx.coroutines.flow.catch
import javax.inject.Inject

class ViewModelRetrofitDI @Inject constructor(val repositoryDI: RetroRepositoryDI) : ViewModel() {

    fun getQuotes() = liveData {
        repositoryDI.getAllQuotes().catch { e->
        }.collect{
            emit(it)
        }
    }
}