package com.example.mykotlinmvvm.retrofitwithdi.di

import com.example.mykotlinmvvm.retrofitwithdi.RetrofitServiceDi
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@Module
class RetrofitModule  {

 @Provides
 fun providesBaseUrl() : String = "https://api.quotable.io"

 private val logger = HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)

 @Provides
 fun okhttpClientBuilder() : OkHttpClient.Builder = OkHttpClient.Builder().addInterceptor(logger)

 @Provides
 fun providesRetrofitBuilder(baseUrl : String, okHttpClient: OkHttpClient.Builder) :Retrofit = Retrofit.Builder()
  .baseUrl(baseUrl)
  .addConverterFactory(GsonConverterFactory.create())
  .client(okHttpClient.build()).build()

 @Provides
 fun providesQuotesApis(retrofit: Retrofit) : RetrofitServiceDi = retrofit.create(RetrofitServiceDi::class.java)

}