package com.example.mykotlinmvvm.mvvmretrofit

import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface QuoteService {

    @GET("/quotes")
    suspend fun getQuotes(): Response<QuoteModel>

//    @POST("readsingleaddress.php")
//    suspend fun getAddress(@Body addressRequiestModel: AddressRequiestModel): Response<AddressResponseModel>

}