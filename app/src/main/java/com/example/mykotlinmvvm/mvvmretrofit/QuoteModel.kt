package com.example.mykotlinmvvm.mvvmretrofit

data class QuoteModel(
    val count: Int,
    val lastItemIndex: Int,
    val page: Int,
    val results: List<Result>,
    val totalCount: Int,
    val totalPages: Int
)