package com.example.mykotlinmvvm.mvvmretrofit

import androidx.lifecycle.MutableLiveData

class QuotesRepository(val quotesService: QuoteService) {

    val quotesLiveData = MutableLiveData<QuoteModel>()
//    val addressLiveData = MutableLiveData<AddressResponseModel>()

    suspend fun getQuotes(){

        val result = quotesService.getQuotes()

        if (result?.body()!=null){
            quotesLiveData.postValue(result.body())
        }

    }


//    suspend fun getAddress(addressRequiestModel: AddressRequiestModel){
//
//        val result = quotesService.getAddress(addressRequiestModel)
//
//        if (result?.body()!=null){
//            addressLiveData.postValue(result.body())
//        }
//
//    }

}

