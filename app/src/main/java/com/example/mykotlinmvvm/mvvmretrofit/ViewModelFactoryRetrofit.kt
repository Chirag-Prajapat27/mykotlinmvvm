package com.example.mykotlinmvvm.mvvmretrofit

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import java.lang.IllegalArgumentException

class ViewModelFactoryRetrofit(val repository: QuotesRepository) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
         if (modelClass.isAssignableFrom(ViewModelRetrofit::class.java)) {
            return ViewModelRetrofit(repository) as T

        }
        throw IllegalArgumentException ("UnknownViewModel")
    }
}