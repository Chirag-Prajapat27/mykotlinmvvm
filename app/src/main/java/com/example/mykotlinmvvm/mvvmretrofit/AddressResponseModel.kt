package com.example.mykotlinmvvm.mvvmretrofit

data class AddressResponseModel(val status:String,val result: List<AddressResult>){

    data class AddressResult(val id:String, val delivery_name:String)

}
