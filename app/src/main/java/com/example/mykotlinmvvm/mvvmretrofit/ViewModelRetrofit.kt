package com.example.mykotlinmvvm.mvvmretrofit

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ViewModelRetrofit(val repository: QuotesRepository): ViewModel() {

    init {
        viewModelScope.launch(Dispatchers.IO) {
            repository.getQuotes()
        }
//        viewModelScope.launch(Dispatchers.IO) {
//            repository.getAddress(AddressRequiestModel("6"))
//        }
    }
}