package com.example.mykotlinmvvm.mvvmretrofit

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitHelper {

    const val BASE_URL = "https://api.quotable.io"
//    const val BASE_URL = "https://ankurupadhyay.com/kdot/apis/user/staging/"

    fun getInstance():Retrofit{
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create()).build()

    }

}