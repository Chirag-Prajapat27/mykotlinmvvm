package com.example.mykotlinmvvm.retrifit

import com.example.mykotlinmvvm.mvvmretrofit.QuoteModel
import com.google.gson.annotations.SerializedName
import retrofit2.Call
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

const val BASE_URL = "https://api.quotable.io"

interface QuotesInterface {


    @GET("/quotes")
//    @SerializedName("tags")
      fun getAllQuotes() : Call<QuoteModel>

}

object QuoteServiceInter{
    val quotesInterface : QuotesInterface
    init {
        val retrofit = Retrofit.Builder().baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        quotesInterface = retrofit.create(QuotesInterface::class.java)
    }

}

