package com.example.mykotlinmvvm.myofficework

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.mykotlinmvvm.databinding.AdapterMovieBinding

class MovieAdapter : RecyclerView.Adapter<MovieAdapter.MyViewHolder>()  {

    var movie = mutableListOf<Movie>()

    fun setMovieList(movie: List<Movie>){
        this.movie = movie.toMutableList()
        notifyDataSetChanged()
    }


class MyViewHolder(val binding: AdapterMovieBinding) : RecyclerView.ViewHolder(binding.root){

}

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = AdapterMovieBinding.inflate(inflater,parent,false)

        return MyViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val movie = movie[position]
        holder.binding.tvName.text = movie.name
        Glide.with(holder.itemView.context).load(movie.imageUrl).into(holder.binding.imageView)

    }

    override fun getItemCount() = movie.size
}