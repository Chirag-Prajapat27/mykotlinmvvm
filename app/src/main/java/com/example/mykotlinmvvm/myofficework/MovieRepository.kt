package com.example.mykotlinmvvm.myofficework

class MovieRepository constructor(val retroService: RetroService) {

    fun getAllMovies() = retroService.getAllMovies()
}