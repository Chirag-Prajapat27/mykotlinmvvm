package com.example.mykotlinmvvm.myofficework

import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

interface RetroService {

    @GET("movielist.json")
    fun getAllMovies(): Call<List<Movie>>


    companion object{
        var retroService: RetroService? = null

        fun getInstance(): RetroService {
            if (retroService ==  null){
                val retrofit = Retrofit.Builder()
                    .baseUrl("https://howtodoandroid.com/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()

                retroService = retrofit.create(RetroService::class.java)
            }
            return retroService!!
        }
    }
}