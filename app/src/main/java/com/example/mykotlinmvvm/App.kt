package com.example.mykotlinmvvm

import android.app.Application
import com.example.mykotlinmvvm.shareprefrencesdagger2.DaggerSPComponents
import com.example.mykotlinmvvm.shareprefrencesdagger2.SPComponents

class App : Application() {

    lateinit var spComponents: SPComponents
    override fun onCreate() {
        super.onCreate()
        spComponents = DaggerSPComponents.factory().create(this)
    }

}