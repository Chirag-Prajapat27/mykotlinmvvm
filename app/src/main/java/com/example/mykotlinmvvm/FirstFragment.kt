package com.example.mykotlinmvvm

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import androidx.fragment.app.Fragment
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.example.mykotlinmvvm.databinding.BaseAdapter
import com.example.mykotlinmvvm.databinding.FragmentFirstBinding
import com.example.mykotlinmvvm.databinding.MyViewModel
import com.example.mykotlinmvvm.databinding.model.ProgramingModel
import com.example.mykotlinmvvm.mvvmretrofit.QuoteModel
import com.example.mykotlinmvvm.retrifit.QuoteServiceInter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class FirstFragment : Fragment() {

    lateinit var binding: FragmentFirstBinding

    lateinit var navController: NavController
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

//        binding = DataBindingUtil.setContentView<FragmentFirstBinding>(requireActivity(),R.layout.fragment_first)
//        val view = ViewModelProvider(this)[MyViewModel::class.java]

        navController = findNavController()

    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
//        binding = DataBindingUtil.setContentView<FragmentFirstBinding>(requireActivity(),R.layout.fragment_first)
        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_first,container,false)
        val view = ViewModelProvider(this)[MyViewModel::class.java]
        binding.myViewModel = view
        binding.lifecycleOwner = this
        binding.thisFragment = this

        binding.btnFloating.setOnClickListener {
            findNavController().navigate(FirstFragmentDirections.actionFirstFragmentToViewDataFragment())
        }

        binding.btnMvvm.setOnClickListener {
            findNavController().navigate(FirstFragmentDirections.actionFirstFragmentToRetrofitMvvmFragment())
        }

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val p1 = ProgramingModel(1, "Java")
        val p2 = ProgramingModel(2, "Kotlin")
        val p3 = ProgramingModel(3, "Android")


        val adapter = BaseAdapter()
        binding.recycleViewFragment.adapter = adapter
        adapter.submitList(listOf(p1,p2,p3))
        adapter.update = {

        }

//        binding.recycleViewFragment.adapter = adapter

//        val database =DataBaseEmployee.getDataBase(requireContext())
//        GlobalScope.launch {
//            database.daoEmployee().  insertEmployeeData(EmployeeModel("Chirag",18000))
//        }
//
//        btnClick.setOnClickListener {
//            database.daoEmployee().getEmployeeData().observe(viewLifecycleOwner) {
//                val adapter = AdapterEmployee(it)
//                recycleViewFragment.adapter = adapter
//            }
//        }

    }

    fun addProduct(){

        val quotes =  QuoteServiceInter.quotesInterface.getAllQuotes()

        quotes.enqueue(object: Callback<QuoteModel> {
            override fun onResponse(call: Call<QuoteModel>, response: Response<QuoteModel>) {

                if (response.body() != null){
                    Toast.makeText(requireContext(), "Success", Toast.LENGTH_SHORT).show()
                    Log.d("Retro",response.body().toString())
                }
            }

            override fun onFailure(call: Call<QuoteModel>, t: Throwable) {
                Log.d("Retro",t.message.toString())
                Toast.makeText(requireContext(), "Error ${t.message}", Toast.LENGTH_SHORT).show()
            }

        })
        Toast.makeText(requireContext(), "It's work", Toast.LENGTH_SHORT).show()


    }

}