package com.example.mykotlinmvvm.interceptor.retrofit

import com.example.mykotlinmvvm.mvvmretrofit.QuoteModel
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.GET

interface RetrofitService {

    @GET("/quotes")
    fun getQuotes(): Call<QuoteModel>

    @GET("/quotes")
    suspend fun getQuotesRespose(): Response<QuoteModel>

}