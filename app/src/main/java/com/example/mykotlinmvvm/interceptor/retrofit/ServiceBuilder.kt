package com.example.mykotlinmvvm.interceptor.retrofit

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
object ServiceBuilder {

    val logger = HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)
    val okHttp = OkHttpClient.Builder().addInterceptor(logger)

    //create retrofit builder
    val builder  = Retrofit.Builder().baseUrl("https://api.quotable.io")
        .addConverterFactory(GsonConverterFactory.create()).client(okHttp.build())

    val retrofit = builder.build()

    fun <T> buildService(serviceType:Class<T>):T{
        return retrofit.create(serviceType)
    }

}