package com.example.mykotlinmvvm.interceptor

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.example.mykotlinmvvm.R
import com.example.mykotlinmvvm.interceptor.retrofit.*
import com.example.mykotlinmvvm.mvvmretrofit.QuoteModel
import com.example.mykotlinmvvm.mvvmretrofit.QuoteService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ActivityInterceptor : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_inter_sefter)

        val retrofit = ServiceBuilder.buildService(RetrofitService::class.java)

        val quoteService = ServiceBuilder.buildService(QuoteService::class.java)

        val repository = InterceptorRepository(quoteService)

        val viewmodel = ViewModelProvider(this,InterceptorFactory(repository))[InterceptorViewModel::class.java]

        viewmodel.quotes.observe(this, Observer {

            Log.d("Data","$it")

        })



        retrofit.getQuotes().enqueue(object : Callback<QuoteModel> {

            override fun onResponse(call: Call<QuoteModel>, response: Response<QuoteModel>) {
                Log.d("Data",response.toString())
            }
            override fun onFailure(call: Call<QuoteModel>, t: Throwable) {
                Log.d("Data",t.message.toString())
            }

        })

    }
}