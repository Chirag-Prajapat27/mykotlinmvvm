package com.example.mykotlinmvvm.interceptor.retrofit

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import java.lang.IllegalArgumentException


class InterceptorFactory(private val repository: InterceptorRepository ) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(InterceptorViewModel::class.java))
        return InterceptorViewModel(repository) as T

        throw IllegalArgumentException ("UnknownViewModel")
    }

}