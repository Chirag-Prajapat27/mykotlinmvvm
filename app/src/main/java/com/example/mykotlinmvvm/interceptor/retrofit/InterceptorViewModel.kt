package com.example.mykotlinmvvm.interceptor.retrofit

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.mykotlinmvvm.mvvmretrofit.QuoteModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class InterceptorViewModel(val repository: InterceptorRepository) : ViewModel() {

    init {
        viewModelScope.launch(Dispatchers.IO) {
            repository.getQuotes()
        }
    }

    val quotes : LiveData<QuoteModel>
    get() = repository.quote

}