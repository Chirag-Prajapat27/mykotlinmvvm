package com.example.mykotlinmvvm.interceptor.retrofit

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.mykotlinmvvm.mvvmretrofit.QuoteModel
import com.example.mykotlinmvvm.mvvmretrofit.QuoteService

class InterceptorRepository(private val quoteResponse : QuoteService) {

    private val quoteLiveData = MutableLiveData<QuoteModel>()

    val quote : LiveData<QuoteModel>
    get() = quoteLiveData

    suspend fun getQuotes(){

        val result = quoteResponse.getQuotes()
        if (result?.body()!= null){
            quoteLiveData.postValue(result.body())
        }

    }

}