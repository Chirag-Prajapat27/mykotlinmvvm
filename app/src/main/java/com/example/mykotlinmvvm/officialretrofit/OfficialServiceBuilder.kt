package com.example.mykotlinmvvm.officialretrofit

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object OfficialServiceBuilder {

    const val BASE_URL = "https://api.quotable.io"
    val logger = HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)
    val okHttp = OkHttpClient.Builder().addInterceptor(logger)

    //create retrofit builder
    val builder = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create()).client(okHttp.build()).build()


    fun <T> buildService(serviceType: Class<T>): T {
        return OfficialServiceBuilder.builder.create(serviceType)
    }


}