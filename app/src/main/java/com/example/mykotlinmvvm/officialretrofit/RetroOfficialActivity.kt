package com.example.mykotlinmvvm.officialretrofit

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import com.example.mykotlinmvvm.R

class RetroOfficialActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_retro_official)

        val service = OfficialServiceBuilder.buildService(RetroService::class.java)
        val viewMode = ViewModelProvider(this,ViewModelFactory(RetroRepository(service)))[VIewModelRetro::class.java]

        viewMode.getQuote().observe(this){
            Toast.makeText(this, "$it", Toast.LENGTH_SHORT).show()
        }

    }
}