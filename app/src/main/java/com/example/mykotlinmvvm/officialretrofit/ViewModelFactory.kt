package com.example.mykotlinmvvm.officialretrofit

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class ViewModelFactory(val quoteRepository: RetroRepository) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
       return VIewModelRetro(quoteRepository) as T
    }
}