package com.example.mykotlinmvvm.officialretrofit

import com.example.mykotlinmvvm.mvvmretrofit.QuoteModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class RetroRepository(val service: RetroService) {

    suspend fun getAllQuotes(): Flow<QuoteModel> = flow {
        val response = service.getQuotes()
        emit(response)
    }

}