package com.example.mykotlinmvvm.officialretrofit

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import kotlinx.coroutines.flow.catch


class VIewModelRetro(val repository: RetroRepository) : ViewModel() {

    fun getQuote() = liveData {
        repository.getAllQuotes().catch { e->

        }.collect{
            emit(it)
        }
    }

}