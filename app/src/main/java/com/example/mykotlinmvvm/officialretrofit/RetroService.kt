package com.example.mykotlinmvvm.officialretrofit

import com.example.mykotlinmvvm.mvvmretrofit.QuoteModel
import retrofit2.http.GET

interface RetroService {

    @GET("/quotes")
    suspend fun getQuotes(): QuoteModel
}