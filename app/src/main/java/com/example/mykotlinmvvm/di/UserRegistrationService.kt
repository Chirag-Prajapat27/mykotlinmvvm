package com.example.mykotlinmvvm.di

import com.example.mykotlinmvvm.di.anotation.SQLQualyfier
import javax.inject.Inject
import javax.inject.Named

class UserRegistrationService @Inject
constructor(private val notificationService: NotificationService, @SQLQualyfier private val userRepository: UserRepository) {

    fun registerUser(email:String, password: String){

        userRepository.saveUser(email, password)
        notificationService.send(email, "chirag", "Hello")
    }

}