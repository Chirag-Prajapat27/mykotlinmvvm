package com.example.mykotlinmvvm.di

import android.util.Log
import javax.inject.Inject

interface NotificationService
{
    fun send( email:String,from:String,body:String)
}

class EmailService @Inject constructor():NotificationService {

    override fun send( email:String,from:String,body:String){

        Log.d("MYmsg3","Email send successfully")
    }
}

class MessageService : NotificationService {
    override fun send(email: String, from: String, body: String) {

        Log.d("MYmsg4","Message send successfully")
    }

}

