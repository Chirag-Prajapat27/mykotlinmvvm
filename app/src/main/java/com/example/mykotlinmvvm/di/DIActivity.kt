package com.example.mykotlinmvvm.di

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.mykotlinmvvm.R
import javax.inject.Inject

class DIActivity : AppCompatActivity() {

    @Inject
    lateinit var emailService: EmailService

    @Inject
    lateinit var userRegistrationService: UserRegistrationService


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_diactivity)

        //component Builder for using component
        val component = DaggerDIComponent.builder().build()

        component.inject(this)

        emailService.send("Chirag@gmial.in","chirag","Hello")
        userRegistrationService.registerUser("chirag@gmail.in","password")


//        val usrRegi = component.getUserRegistrationService()
//        usrRegi.registerUser("chirag@gmail.in","password")

    }
}