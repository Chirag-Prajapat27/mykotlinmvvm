package com.example.mykotlinmvvm.di

import android.util.Log
import javax.inject.Inject

interface UserRepository{
    fun saveUser(email:String, password:String)
}

//constructor injection
class SQLRepository @Inject constructor() : UserRepository{

    override fun saveUser(email:String, password:String){
        Log.d("MYmsg", "User save Successfully")
        Log.d("MYmsg", "$email $password")
    }
}

//
class FireBaseRepository : UserRepository{

    override fun saveUser(email: String, password: String) {
        Log.d("MYmsg1", "User save FireBase Successfully")
        Log.d("MYmsg2", "$email $password")
    }


}

