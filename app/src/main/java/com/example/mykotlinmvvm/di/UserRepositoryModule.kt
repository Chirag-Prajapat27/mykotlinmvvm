package com.example.mykotlinmvvm.di

import androidx.navigation.Navigator
import com.example.mykotlinmvvm.di.anotation.FireBaseQualyFier
import com.example.mykotlinmvvm.di.anotation.SQLQualyfier
import dagger.Binds
import dagger.Module
import dagger.Provides
import javax.inject.Named

@Module
 class UserRepositoryModule {

    //Normal
    @FireBaseQualyFier
    @Provides
    fun getFireBaseRepository() :UserRepository{
        return FireBaseRepository()
    }


    //Normal
//    @Named("sql")  //using with default method

    @SQLQualyfier
    @Provides
    fun getSQLRepository() :UserRepository{
        return SQLRepository()
    }

//    // Binds (only use when class as @Inject Annotation)
//    @Binds
//    abstract fun getSQLRepository(sqlRepository: SQLRepository) : UserRepository

}