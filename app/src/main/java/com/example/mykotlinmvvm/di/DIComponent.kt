package com.example.mykotlinmvvm.di

import dagger.Component
import javax.inject.Named

//for fill injection

@Component(modules = [NotificationServiceModual::class, UserRepositoryModule::class])
 interface DIComponent {

    //for constructor injection
    fun getEmail():EmailService
    fun getUserRegistrationService():UserRegistrationService

    //fill injection
    fun inject(activity: DIActivity)

}