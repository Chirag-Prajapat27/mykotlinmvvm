package com.example.mykotlinmvvm.genrics

import android.content.Context
import android.content.Intent
import com.google.gson.Gson

inline fun <reified T> String.stringToJson() : T = Gson().fromJson(this,T::class.java)

fun <T> jsonToString (value: T) = Gson().toJson(value)

inline fun <reified T: Any> createIntent(context: Context) = Intent(context,T::class.java)

inline fun <reified T:Any> Context.launchActivity() {
    val intent = createIntent<T>(this)
    startActivity(intent)
}