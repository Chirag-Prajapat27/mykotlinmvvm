package com.example.mykotlinmvvm.genrics

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.mykotlinmvvm.MainActivity
import com.example.mykotlinmvvm.R
import com.example.mykotlinmvvm.test.TestModel
import com.google.gson.Gson

class GenericsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_genrics)

        val data = Gson().toJson(TestModel(1,"Chirag"))
        data.stringToJson<TestModel>()

        jsonToString(TestModel(1,"Chirag")).stringToJson<TestModel>()

        launchActivity<MainActivity>()

    }
}