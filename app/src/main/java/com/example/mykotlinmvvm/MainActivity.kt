package com.example.mykotlinmvvm

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import com.example.mykotlinmvvm.databinding.ActivityMainBinding
import com.example.mykotlinmvvm.databinding.MyViewModel

class MainActivity : AppCompatActivity() {

    lateinit var controller: NavController
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

//        val binding = DataBindingUtil.setContentView<ActivityMainBinding>(this,R.layout.activity_main)
//        val myViewModel = ViewModelProvider(this)[MyViewModel::class.java]
//        binding.viewModel = myViewModel
//


        val navHostFragment =supportFragmentManager.findFragmentById(R.id.nav_host_fragment_container) as NavHostFragment
        controller = navHostFragment.findNavController()


    }
}