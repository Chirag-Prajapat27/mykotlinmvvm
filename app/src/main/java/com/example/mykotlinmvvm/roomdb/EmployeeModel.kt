package com.example.mykotlinmvvm.roomdb

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "employee_table")
data class EmployeeModel(@ColumnInfo(name = "emp_name") val emp_Name: String,
                         @ColumnInfo(name = "emp_salary") val emp_Salary: Int,
                         @PrimaryKey(autoGenerate = true) val id:Int = 0 )
