package com.example.mykotlinmvvm.roomdb

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = arrayOf(EmployeeModel::class), exportSchema = false, version = 1)
abstract class DataBaseEmployee : RoomDatabase() {

    abstract fun daoEmployee() : DaoEmployee

    companion object{

        @Volatile
        var INSTANCE : DataBaseEmployee? = null

        fun  getDataBase(context: Context): DataBaseEmployee{
            if (INSTANCE != null){
                synchronized(this){
                    INSTANCE = Room.databaseBuilder(context.applicationContext,
                    DataBaseEmployee::class.java, "employee_database").build()
                }
            }
            return INSTANCE!!
        }


    }

}