package com.example.mykotlinmvvm.roomdb

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.mykotlinmvvm.databinding.EmployeeViewBinding

class AdapterEmployee(list: List<EmployeeModel>) : RecyclerView.Adapter<AdapterEmployee.MyViewHolderEmployee>() {

    var employeeList: List<EmployeeModel> = list

//    class MyViewHolderEmployee(itemView: View) : RecyclerView.ViewHolder(itemView)
    class MyViewHolderEmployee(binding : EmployeeViewBinding) : RecyclerView.ViewHolder(binding.root){
    val myBinding = binding
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolderEmployee {
//        val view =  LayoutInflater.from(parent.context).inflate(R.layout.employee_view,parent,false)
        val view = EmployeeViewBinding.inflate(LayoutInflater.from(parent.context),parent,false)

        return MyViewHolderEmployee(view)
    }

    override fun onBindViewHolder(holder: MyViewHolderEmployee, position: Int) {

        holder.myBinding.empModel = employeeList[position]

//        holder.itemView.tvName.text = employeeList[position].emp_Name
//        holder.itemView.tvEmpSalary.text = employeeList[position].emp_Salary.toString()
    }

    override fun getItemCount() = employeeList.size
}