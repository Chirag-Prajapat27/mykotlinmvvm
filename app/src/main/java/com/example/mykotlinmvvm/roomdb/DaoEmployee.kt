package com.example.mykotlinmvvm.roomdb

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query

@Dao
interface DaoEmployee {

@Query("Select * From employee_table")
 fun getEmployeeData() : LiveData<List<EmployeeModel>>

 @Insert
 suspend fun insertEmployeeData(employee : EmployeeModel)

 @Delete
 suspend fun deleteEmployeeData(employee : EmployeeModel)

}